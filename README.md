# funniest

This repository is created for the purpose to learn how to create a command-line tool for my Python package.

Funniest
--------

To use (with caution), simply do::

    >>> import funniest
    >>> print funniest.joke()

Link to the official documentation:

[Command Line Scripts](https://python-packaging.readthedocs.io/en/latest/command-line-scripts.html)