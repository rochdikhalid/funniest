from setuptools import setup

def readme():
      with open('README.md') as f:
            return f.read()

setup(
      name = 'funniest',
      version = '0.1',
      description = 'created to understand how to build a command-line tool for my package',
      long_description = readme(),
      classifiers = [
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Topic :: Text Processing :: Linguistic',
      ],
      url = 'http://github.com/rochdikhalid/funniest',
      author = 'Rochdi Khalid',
      author_email = 'rochdib.khalid@gmail.com',
      license = 'MIT',
      packages = ['funniest'],
      install_requires = [
          'markdown',
      ],
      entry_points = {
        'console_scripts': ['funniest-joke=funniest.command_line:main'],
      },
      test_suite = 'nose.collector',
      tests_require = ['nose'],
      include_package_data = True,
      zip_safe = False
)