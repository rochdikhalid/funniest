# Notes 

To install the package locally for use on our system:

```
pip install .
```

To install the package locally so the changes to the source files will be available as well to other users of our package on our system (install with a symlink):

```
pip install -e .
```

If we add any dependency to our package, and we want to prove this works:

```
python setup.py develop
```
The dependency we will be installed as well.

What `nose` is used for?

## Adding command line scripts

### The `scripts` keyword argument

1. Write a script in a seperate file `funniest-joke`:

```
#!/usr/bin/env python

import funniest
print(funniest.joke())
```
2. Then, declare the script in `setup()`:

```
setup(
    ...
    scripts = ['bin/funniest-joke'],
    ...
)
```
3. Next, install the package, and run: 

```
funniest-joke
```
Setuptools will copy the script to our PATH and make it available for general use.

### The `console_scripts` entry point

1. Add a new file:

```
funniest/
    funniest/
        __init__.py
        command_line.py
        ...
    setup.py
    ...
```
2. Then, add a new function to support the command line tool:

```
import funniest

def main():
    print(funniest.joke())
```
3. You can test the script by running:

```
$ python
>>> import funniest.command_line
>>> funniest.command_line.main()
...
```
4. Next, you can register the `main()` function in `setup()`:

```
setup(
    ...
    entry_points = {
        'console_scripts': ['funniest-joke=funniest.command_line:main'],
    }
    ...
)
```
5. Next, install the package, and run: 

```
funniest-joke
```
Setuptools will generate a standalone script ‘shim’ which imports your module and calls the registered function.